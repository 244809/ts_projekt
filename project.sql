CREATE DATABASE nfz;
USE nfz;

-- -----------------------------------------------------
-- Table Przychodnia
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Przychodnia (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  nazwa VARCHAR(45) NULL,
  telefon VARCHAR(12)NULL,
  adres VARCHAR(45) NULL
  );
-- -----------------------------------------------------
-- Table Lekarz
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Lekarz (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  imie VARCHAR(45) NULL,
  nazwisko VARCHAR(45) NULL,
  specjalizacja VARCHAR(45) NULL,
  przychodnia_id INT NOT NULL,
    FOREIGN KEY (przychodnia_id) REFERENCES Przychodnia (id)
);
-- -----------------------------------------------------
-- Table Pacjent
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Pacjent (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  imie VARCHAR(45) NULL,
  nazwisko VARCHAR(45) NULL,
  telefon VARCHAR(11)NULL,
  login VARCHAR(45) NULL UNIQUE,
  haslo VARCHAR(45) NULL
  );  
-- -----------------------------------------------------
-- Table Wolne
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Wolne (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  data DATE NULL,
  czas TIME NULL,
  lekarz_id INT NOT NULL,
    FOREIGN KEY (lekarz_id) REFERENCES Lekarz (id)
);
-- -----------------------------------------------------
-- Table Zajete
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS Zajete (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    data DATE NULL,
    czas TIME NULL,
  lekarz_id INT NOT NULL,
      FOREIGN KEY (lekarz_id) REFERENCES Lekarz (id),
  pacjent_id INT NOT NULL,
    FOREIGN KEY (pacjent_id) REFERENCES Pacjent (id),
      status ENUM('nadchodząca', 'odbyta') NULL
    );


-- -----------------------------------------------------
-- Przykładowe dane
-- -----------------------------------------------------    
INSERT INTO przychodnia(nazwa, telefon, adres) VALUES 
('CENTRUM MEDYCZNE KOL-MED', '71 723 07 85','Akacjowa 8 Wrocław'),
('CENTRUM MEDYCZNE KOL-MED', '77 402 03 68','Krakowska 124 Opole'),
('CENTRUM MEDYCZNE KOL-MED', '13 437 15 58','Lewakowskiegp 49 Krosno')
;
INSERT INTO lekarz(imie, nazwisko, specjalizacja,przychodnia_id) VALUES 
('Adam', 'Kowalczyk','dentysta',3),
('Joanna', 'Mleczarz','ortopeda',2),
('Karol', 'Lech','okulista',1),
('Tomasz', 'Szal', 'dentysta',1),
('Laura', 'Jankowska', 'okulista',3)
;
  INSERT INTO pacjent(imie, nazwisko, telefon,login, haslo) VALUES 
('Adrian', 'Kowalski', '772 345 775', 'adi123', 'haslo1'),
('Marian', 'Nowak', '345 455 667', 'mari123', 'haslo2'),
('Renata', 'Filip', '211 223 456', 'renia123', 'haslo3'),
('Rafał', 'Rysz', '331 221 406', 'rysz123', 'haslo4'),
('Anna', 'Turczyn', '112 234 565', 'ania123', 'haslo5')
;
  INSERT INTO wolne(data,czas,lekarz_id) VALUES
('2020-05-16','8:15',1),
('2020-05-18','8:15',5),
('2020-05-19','10:15',2),
('2020-05-22','08:15',1),
('2020-05-22','10:15',1),
('2020-05-22','12:15',1),
('2020-05-23','10:15',5),
('2020-05-23','10:15',2),
('2020-05-23','12:15',2),
('2020-05-23','12:15',5),
('2020-05-23','12:15',1),
('2020-05-24','15:15',1),
('2020-05-24','15:15',2),
('2020-05-24','10:15',3),
('2020-05-24','15:15',3),
('2020-05-24','10:15',4),
('2020-05-24','10:15',2),
('2020-05-24','12:15',4),
('2020-05-24','15:15',4)
;
INSERT INTO zajete(data,czas,lekarz_id,pacjent_id,status) VALUES
  ('2020-05-02', '8:15',1,2,'odbyta'),
  ('2020-05-02', '8:15',2,5,'odbyta'),
  ('2020-05-05', '8:15',3,4,'odbyta'),
  ('2020-05-06', '10:15',5,1,'odbyta'),
  ('2020-05-07', '8:15',4,3,'odbyta'),
  ('2020-05-12', '10:15',1,1,'odbyta'),
  ('2020-05-12', '10:15',5,2,'odbyta'),
  ('2020-05-13', '10:15',5,2,'nadchodzaca'),
  ('2020-05-16', '10:15',3,4,'nadchodząca'),
  ('2020-05-18', '10:15',1,2,'nadchodząca'),
  ('2020-05-19', '10:15',3,4,'nadchodząca'),
  ('2020-05-19', '10:15',5,1,'nadchodząca'),
  ('2020-05-20', '12:15',5,2,'nadchodząca'),
  ('2020-05-20', '15:15',1,1,'nadchodząca'),
  ('2020-05-20', '15:15',2,5,'nadchodząca'),
  ('2020-05-20', '15:15',4,3,'nadchodząca'),
  ('2020-05-21', '10:15',2,5,'nadchodząca'),
  ('2020-05-21', '10:15',1,2,'nadchodząca'),
  ('2020-05-21', '10:15',5,1,'nadchodząca')
;    

-- -----------------------------------------------------
-- Widoki
-- -----------------------------------------------------   
    CREATE VIEW wizyty AS SELECT 
    z.id, z.data, z.czas, z.status, z.pacjent_id,
    p.nazwa as przychodnia, p.adres as adres ,
    concat('lek. ',l.imie,' ', l.nazwisko) as lekarz, l.specjalizacja
	FROM lekarz AS l INNER JOIN przychodnia AS p ON l.przychodnia_id = p.id INNER JOIN zajete AS z ON z.lekarz_id = l.id;
    
	CREATE VIEW terminy AS SELECT 
    w.id, w.data,w.czas, 
    p.nazwa as przychodnia, p.adres as adres ,
    concat('lek. ',l.imie,' ', l.nazwisko) as lekarz, l.specjalizacja
	FROM lekarz AS l INNER JOIN przychodnia AS p ON l.przychodnia_id = p.id INNER JOIN wolne AS w ON w.lekarz_id = l.id;
    
-- -----------------------------------------------------
-- Triggery
-- -----------------------------------------------------       
delimiter $$
create trigger zajmowanie
after insert
on zajete for each row
begin
declare lek_id int default (select lekarz_id from zajete where id=new.id);
declare dat date default (select data from zajete where id=new.id);
declare cz time default (select czas from zajete where id=new.id);
delete from wolne where lekarz_id=lek_id and data=dat and czas=cz;
end $$
delimiter ;

delimiter $$
create trigger zwalnianie
before delete
on zajete for each row
begin
declare lek_id int default (select lekarz_id from zajete where id=old.id);
declare dat date default (select data from zajete where id=old.id);
declare cz time default (select czas from zajete where id=old.id);
insert into wolne(data,czas,lekarz_id) value (dat,cz,lek_id);
end $$
delimiter ;

-- -----------------------------------------------------
-- Procedury
-- -----------------------------------------------------   
DELIMITER $$
create procedure updateVisitStatus()
begin
declare visit_max int default(select max(id) from zajete);
declare visit_min int default(select min(id) from zajete);
declare visit_status varchar(30) default(select status from zajete where id=visit_min);
declare visit_date date default(select data from zajete where id=visit_min);
declare visit_time time default(select czas from zajete where id=visit_min);
declare today_diff int default(select to_seconds(current_date()) + time_to_sec(current_time()) - to_seconds(visit_date) - time_to_sec(visit_time));
simple_loop: LOOP
if(today_diff>0 and visit_status='nadchodząca') then
update zajete set status='odbyta' where id=visit_min;
end if;
select visit_min+1 into visit_min;
select status from zajete where id=visit_min into visit_status;
select data from zajete where id=visit_min into visit_date;
select czas from zajete where id=visit_min into visit_time;
select to_seconds(current_date()) + time_to_sec(current_time()) - to_seconds(visit_date) - time_to_sec(visit_time) into today_diff;
IF visit_max<visit_min THEN
            LEAVE simple_loop;
         END IF;
END LOOP simple_loop;
end $$
DELIMITER ;

DELIMITER $$
create procedure updateTermins()
begin
declare termin_max int default(select max(id) from wolne);
declare termin_min int default(select min(id) from wolne);
declare termin_date date default(select data from wolne where id=termin_min);
declare termin_time time default(select czas from wolne where id=termin_min);
declare today_diff int default(select to_seconds(current_date()) + time_to_sec(current_time()) - to_seconds(termin_date) - time_to_sec(termin_time));
simple_loop: LOOP
if(today_diff>0) then
delete from wolne where id=termin_min;
end if;
select termin_min+1 into termin_min;
select data from wolne where id=termin_min into termin_date;
select czas from wolne where id=termin_min into termin_time;
select to_seconds(current_date()) + time_to_sec(current_time()) - to_seconds(termin_date) - time_to_sec(termin_time) into today_diff;
IF termin_max<termin_min THEN
            LEAVE simple_loop;
         END IF;
END LOOP simple_loop;
end $$
DELIMITER ;

-- -----------------------------------------------------
-- Użytkownik (niezalogowany pacjent)
-- -----------------------------------------------------   
drop user 'pacjent'@'localhost';
CREATE USER 'pacjent'@'localhost' IDENTIFIED BY 'pacjent'; 
GRANT insert ON `pacjent` TO 'pacjent'@'localhost';
GRANT select ON `terminy` TO 'pacjent'@'localhost';
GRANT select ON `pacjent` TO 'pacjent'@'localhost';

