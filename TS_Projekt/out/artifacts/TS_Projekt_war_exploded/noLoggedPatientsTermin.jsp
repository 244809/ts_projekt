<%--
  Created by IntelliJ IDEA.
  User: macmini2
  Date: 08/04/2020
  Time: 12:38
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*,pwr.ib.Widoki.Terminy" %>
<html>
<head>
    <title>Terminy wizyt</title>
    <link rel="icon" href="images/appointment_ico.jpg" type="image/x-icon" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link type="text/css" rel="stylesheet" href="bootstrap.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>


<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="#">@ppointment</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.html">Strona główna</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="NewPatientServlet" method="get">Terminy wizyt<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.html">Zaloguj się</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="registration.html">Zarejestruj się</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>


<h1>Wolne terminy</h1>


<div class="row form-group"></div>
<form action="NewPatientServlet" method="get">
<div class="jumbotron">
    <table class="table table-striped">
        <thead>
    <tr>
        <th>Nazwa miasta: </th>
        <th>Specjalizacja: </th>
        <th>Termin od: </th>
        <th>Termin do: </th>
    </tr>
        </thead>
        <tbody>
    <tr>
        <td><input type="text" name="miasto" placeholder="Miasto" value= ${miasto}></td>
        <td><input type="text" name="specjalizacja" placeholder="Specjalizacja" value= ${specjalizacja}></td>
        <td><input type="datetime-local" name="data_od" value= ${data_od}></td>
        <td><input type="datetime-local" name="data_do" value= ${data_do}></td>
    </tr>
        </tbody>
    </table>
    <input type="submit" value="Szukaj" button type="button" class="btn btn-success"></button>
</div>
</form>


<div class="row form-group"></div>

<table class="table table-striped">

    <thead>
    <tr>
        <th width="20%" scope="col">Przychodnia</th>
        <th scope="col">Adres</th>
        <th scope="col">Lekarz</th>
        <th scope="col">Specjalizacja</th>
        <th scope="col">Data</th>
        <th scope="col">Godzina</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach var="tmpTermins" items="${TERMINS_LIST}">

        <tr>
            <td>${tmpTermins.przychodnia}</td>
            <td>${tmpTermins.adres}</td>
            <td>${tmpTermins.lekarz}</td>
            <td>${tmpTermins.specjalizacja}</td>
            <td>${tmpTermins.data}</td>
            <td>${tmpTermins.czas}</td>
        </tr>

    </c:forEach>
    </tbody>
</table>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>


</html>