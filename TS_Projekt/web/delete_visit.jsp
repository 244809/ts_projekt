<%--
  Created by IntelliJ IDEA.
  User: macmini2
  Date: 08/04/2020
  Time: 12:38
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.*,pwr.ib.Widoki.Wizyty" %>
<html>
<head>
    <title>Panel Pacjenta</title>
    <link rel="icon" href="images/appointment_ico.jpg" type="image/x-icon" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link type="text/css" rel="stylesheet" href="bootstrap.css">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>


<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container">
        <a class="navbar-brand" href="#">@ppointment</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Panel pacjenta<span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="login.html">Wyloguj się</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search">
                <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
            </form>
        </div>
    </div>
</nav>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>


<h1>Twoje nadchodzące wizyty</h1>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>

<table class="table table-striped">

    <thead>
    <tr>
        <th scope="col">Przychodnia</th>
        <th scope="col">Adres</th>
        <th scope="col">Lekarz</th>
        <th scope="col">Specjalizacja</th>
        <th scope="col">Data</th>
        <th scope="col">Godzina</th>
        <th scope="col">Zrezygnuj</th>
    </tr>
    </thead>
    <tbody>

    <c:forEach var="tmpVisit" items="${VISITS_LIST}">

        <c:url var="deleteLink" value="PatientServlet">
            <c:param name="command" value="DELETE"></c:param>
            <c:param name="visitID" value="${tmpVisit.id}"></c:param>
        </c:url>

        <tr>
            <td>${tmpVisit.przychodnia}</td>
            <td>${tmpVisit.adres}</td>
            <td>${tmpVisit.lekarz}</td>
            <td>${tmpVisit.specjalizacja}</td>
            <td>${tmpVisit.data}</td>
            <td>${tmpVisit.czas}</td>
            <td><a href="${deleteLink}"
                   onclick="if(!(confirm('Czy na pewno chcesz usunąć tą wizytę?'))) return false">
                <button type="button" class="btn btn-danger">Usuń wizytę</button>
            </a></td>
        </tr>

    </c:forEach>
    </tbody>
</table>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>


<c:url var="terminsLink" value="PatientServlet">
    <c:param name="command" value="TERMINS"></c:param>
</c:url>

<c:url var="visitsLink" value="PatientServlet">
    <c:param name="command" value="VISITS"></c:param>
</c:url>

<div class="col-sm-9">
    <p><a class="btn btn-primary btn-info" href="${terminsLink}" role="button">Dodaj wizytę</a></p>
</div>
<div class="col-sm-9">
    <p><a class="btn btn-primary btn-info" href="${visitsLink}" role="button">Twoje wizyty</a></p>
</div>

<div class="row form-group"></div>
<div class="row form-group"></div>
<div class="row form-group"></div>

</html>