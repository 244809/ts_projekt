package pwr.ib;

import java.util.ArrayList;

public class Szukanie {

    //parametry wyszukiwania wolnych terminów
    private String miasto;
    private String specjalizacja;
    private String dat_od;
    private String dat_do;

    public Szukanie(String miasto, String specjalizacja, String dat_od, String dat_do) {
        this.miasto = miasto;
        this.specjalizacja = specjalizacja;
        this.dat_od = dat_od;
        this.dat_do = dat_do;
    }

    //metoda zwracająca gotowe zapytanie uwzględniające podane parametry wyszukiwania
    public String query() {
        String query = "select * from terminy";
        ArrayList<String> warunki = new ArrayList<>();
        int warunek = 0;
        if (miasto != null && !miasto.equals("")) {
            warunek++;
            warunki.add("adres like '%" + miasto + "'");
        }
        if (specjalizacja != null && !specjalizacja.equals("")) {
            warunek++;
            warunki.add("specjalizacja= '" + specjalizacja + "'");
        }
        if (dat_od != null && !dat_od.equals("")) {
            warunek++;
            warunki.add("to_seconds(data)+time_to_sec(czas) >= to_seconds('" + dat_od + "')");
        }
        if (dat_do != null && !dat_do.equals("")) {
            warunek++;
            warunki.add("to_seconds(data)+time_to_sec(czas) <= to_seconds('" + dat_do + "')");
        }
        if (warunek > 0) {
            query = query + " where " + warunki.get(0);
            for (int i = 1; i < warunek; i++) {
                query = query + " and " + warunki.get(i);
            }
        }
        return query;
    }

}
