package pwr.ib.Widoki;


import java.sql.Date;
import java.sql.Time;

public class Wizyty {

    private int id;
    private int id_pacjent;
    private String przychodnia;
    private String adres;
    private String lekarz;
    private String specjalizacja;
    private Date data;
    private Time czas;
    private String status;


    public Wizyty(int id, int id_pacjent, String przychodnia, String adres, String lekarz, String specjalizacja, Date data, Time czas, String status) {
        this.id = id;
        this.id_pacjent = id_pacjent;
        this.przychodnia = przychodnia;
        this.adres = adres;
        this.lekarz = lekarz;
        this.specjalizacja = specjalizacja;
        this.data = data;
        this.czas = czas;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_pacjent() {
        return id_pacjent;
    }

    public void setId_pacjent(int id_pacjent) {
        this.id_pacjent = id_pacjent;
    }

    public String getPrzychodnia() {
        return przychodnia;
    }

    public void setPrzychodnia(String przychodnia) {
        this.przychodnia = przychodnia;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getLekarz() {
        return lekarz;
    }

    public void setLekarz(String lekarz) {
        this.lekarz = lekarz;
    }

    public String getSpecjalizacja() {
        return specjalizacja;
    }

    public void setSpecjalizacja(String specjalizacja) {
        this.specjalizacja = specjalizacja;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Time getCzas() {
        return czas;
    }

    public void setCzas(Time czas) {
        this.czas = czas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
