package pwr.ib.Widoki;

import java.sql.Date;
import java.sql.Time;

public class Terminy {

    private int id;
    private String przychodnia;
    private String adres;
    private String lekarz;
    private String specjalizacja;
    private Date data;
    private Time czas;

    public Terminy(int id, String przychodnia, String adres, String lekarz, String specjalizacja, Date data, Time czas) {
        this.id = id;
        this.przychodnia = przychodnia;
        this.adres = adres;
        this.lekarz = lekarz;
        this.specjalizacja = specjalizacja;
        this.data = data;
        this.czas = czas;
    }

    public Terminy(String przychodnia, String adres, String lekarz, String specjalizacja, Date data, Time czas) {
        this.przychodnia = przychodnia;
        this.adres = adres;
        this.lekarz = lekarz;
        this.specjalizacja = specjalizacja;
        this.data = data;
        this.czas = czas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrzychodnia() {
        return przychodnia;
    }

    public void setPrzychodnia(String przychodnia) {
        this.przychodnia = przychodnia;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getLekarz() {
        return lekarz;
    }

    public void setLekarz(String lekarz) {
        this.lekarz = lekarz;
    }

    public String getSpecjalizacja() {
        return specjalizacja;
    }

    public void setSpecjalizacja(String specjalizacja) {
        this.specjalizacja = specjalizacja;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Time getCzas() {
        return czas;
    }

    public void setCzas(Time czas) {
        this.czas = czas;
    }
}
