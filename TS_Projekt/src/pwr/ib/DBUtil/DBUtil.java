package pwr.ib.DBUtil;

import pwr.ib.Widoki.Terminy;

import java.sql.*;
import java.util.List;

public abstract class DBUtil {

    // zamykanie połączenia
    protected static void close(Connection conn, Statement statement, ResultSet resultSet) {

        try {
            if (resultSet != null)
                resultSet.close();
            if (statement != null)
                statement.close();
            if (conn != null)
                conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // wywoływanie procedur typu update z bazy danych
    public void update() throws SQLException {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/nfz?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET", "root", "Student244809");
            connection.prepareCall("CALL updateVisitStatus").executeQuery();
        } finally {
            close(connection, null, null);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/nfz?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET", "root", "Student244809");
            connection.prepareCall("CALL updateTermins").executeQuery();
        } finally {
            close(connection, null, null);
        }

    }

    abstract public boolean searchPatientID();

    abstract public List<Terminy> getTermins(String query) throws Exception;

}


