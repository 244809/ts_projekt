package pwr.ib.DBUtil;

import pwr.ib.Widoki.Terminy;
import pwr.ib.Widoki.Wizyty;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBUtilPatient extends DBUtil {

    private String login;
    private String password;
    private String root;
    private String rootPassword;
    private String URL;
    private int pacjent_id;


    public DBUtilPatient(String URL) {
        this.URL = URL;
    }

    //sprawdznie czy pacjent jest zarejestrowany w bazie danych
    //jeśli tak pobieramy jego id do dalszych operacji
    @Override
    public boolean searchPatientID() {
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            conn = DriverManager.getConnection(URL, root, rootPassword);
            String sql = "SELECT id FROM pacjent where login=" + "'" + login + "'" + " and haslo=" + "'" + password + "'";
            statement = conn.prepareStatement(sql);
            resultSet = statement.executeQuery(sql);
            if (resultSet.next() == true) {
                pacjent_id = resultSet.getInt("id");
            } else {
                pacjent_id = 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, statement, resultSet);
        }
        return pacjent_id != 0;
    }

    //pobieranie listy wolnych terminów - zapytanie pobierające zależne od parametrów wyszukiwania
    @Override
    public List<Terminy> getTermins(String query) throws Exception {
        List<Terminy> terminy = new ArrayList<>();
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            conn = DriverManager.getConnection(URL, root, rootPassword);
            String sql = query;
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String przychodnia = resultSet.getString("przychodnia");
                String adres = resultSet.getString("adres");
                String lekarz = resultSet.getString("lekarz");
                String specjalizacja = resultSet.getString("specjalizacja");
                Date data = resultSet.getDate("data");
                Time czas = resultSet.getTime("czas");
                terminy.add(new Terminy(id, przychodnia, adres, lekarz, specjalizacja, data, czas));
            }
        } finally {
            close(conn, statement, resultSet);
        }
        return terminy;
    }

    //pobieranie listy wizyt wszystkich lub tylko nadchodzących dla pacjenta o danym id
    public List<Wizyty> getVisit(boolean czyWszystkie) throws Exception {

        List<Wizyty> wizyty = new ArrayList<>();
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            conn = DriverManager.getConnection(URL, root, rootPassword);
            String sql;
            if (czyWszystkie) {
                sql = "SELECT * FROM wizyty";
            } else {
                sql = "SELECT * FROM wizyty WHERE status = 'nadchodząca'";
            }
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                int idPacjenta = resultSet.getInt("pacjent_id");
                if (pacjent_id == idPacjenta) {
                    int id = resultSet.getInt("id");
                    String przychodnia = resultSet.getString("przychodnia");
                    String adres = resultSet.getString("adres");
                    String lekarz = resultSet.getString("lekarz");
                    String specjalizacja = resultSet.getString("specjalizacja");
                    Date data = resultSet.getDate("data");
                    Time czas = resultSet.getTime("czas");
                    String status = resultSet.getString("status");
                    wizyty.add(new Wizyty(id, Integer.valueOf(pacjent_id), przychodnia, adres, lekarz, specjalizacja, data, czas, status));
                }
            }
        } finally {
            close(conn, statement, resultSet);
        }
        return wizyty;
    }

    //dodawanie konkretnej, nowej wizyty - przeniesienie z tabeli wolne do zajęte z odpowiednim id pacjenta
    public void addVisit(String id) throws Exception {
        Connection conn = null;
        Statement statement = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int lekarz_id = 0;
        Date data = null;
        Time czas = null;
        try {
            conn = DriverManager.getConnection(URL, root, rootPassword);
            String sql = "SELECT data,czas,lekarz_id FROM wolne WHERE id =" + id;
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                lekarz_id = resultSet.getInt("lekarz_id");
                data = resultSet.getDate("data");
                czas = resultSet.getTime("czas");
            }
            sql = "INSERT INTO zajete(data,czas,lekarz_id,pacjent_id,status) " +
                    "VALUES(?,?,?,?,?)";

            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setDate(1, data);
            preparedStatement.setTime(2, czas);
            preparedStatement.setInt(3, lekarz_id);
            preparedStatement.setInt(4, pacjent_id);
            preparedStatement.setString(5, "nadchodząca");
            preparedStatement.execute();

        } finally {
            close(conn, statement, null);
        }


    }

    //usuwanie konkretnej wizyty - przeniesienie z tabeli zajete z powrotem do wolne
    public void deleteVisit(String id) throws Exception {
        Connection conn = null;
        PreparedStatement statement = null;

        try {
            conn = DriverManager.getConnection(URL, root, rootPassword);
            String sql = "DELETE FROM zajete WHERE id =" + id;
            statement = conn.prepareStatement(sql);
            statement.execute();
        } finally {
            close(conn, statement, null);
        }
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public void setRootPassword(String rootPassword) {
        this.rootPassword = rootPassword;
    }

    public int getPacjent_id() {
        return pacjent_id;
    }
}
