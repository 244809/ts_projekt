package pwr.ib.DBUtil;

import pwr.ib.Pacjent;
import pwr.ib.Widoki.Terminy;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBUtilNewPatient extends DBUtil {

    private Pacjent pacjent;
    private DataSource dataSource;

    public DBUtilNewPatient(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //sprawdznie czy rejestrowany użytkownik nie jest już w bazie danych (po unikalnym loginie)
    //jeśli nie istnieje natępuje jego rejestracja
    @Override
    public boolean searchPatientID() {
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
        boolean isRegister = false;
        try {
            conn = dataSource.getConnection();
            String sql = "SELECT id FROM pacjent where login=" + "'" + pacjent.getLogin() + "'";
            statement = conn.prepareStatement(sql);
            resultSet = statement.executeQuery(sql);
            if (resultSet.next() == false) {
                register();
                isRegister = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            close(conn, statement, resultSet);
        }
        return isRegister;
    }

    //pobieranie listy wolnych terminów - zapytanie pobierające zależne od parametrów wyszukiwania
    @Override
    public List<Terminy> getTermins(String query) throws Exception {
        List<Terminy> terminy = new ArrayList<>();
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            conn = dataSource.getConnection();
            String sql = query;
            statement = conn.createStatement();
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                int id = resultSet.getInt("id");
                String przychodnia = resultSet.getString("przychodnia");
                String adres = resultSet.getString("adres");
                String lekarz = resultSet.getString("lekarz");
                String specjalizacja = resultSet.getString("specjalizacja");
                Date data = resultSet.getDate("data");
                Time czas = resultSet.getTime("czas");
                terminy.add(new Terminy(id, przychodnia, adres, lekarz, specjalizacja, data, czas));
            }
        } finally {
            close(conn, statement, resultSet);
        }
        return terminy;
    }

    //rejestracaj pacjenta
    private void register() throws SQLException {
        Connection conn = null;
        Statement statement = null;
        PreparedStatement preparedStatement = null;

        try {
            conn = dataSource.getConnection();
            String sql = "INSERT INTO pacjent(imie, nazwisko, telefon,login, haslo) " +
                    "VALUES(?,?,?,?,?)";

            preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, pacjent.getImie());
            preparedStatement.setString(2, pacjent.getNazwisko());
            preparedStatement.setString(3, pacjent.getTelefon());
            preparedStatement.setString(4, pacjent.getLogin());
            preparedStatement.setString(5, pacjent.getHaslo());
            preparedStatement.execute();

        } finally {
            close(conn, statement, null);
        }
    }

    //metoda set do przekazywania danych pacjenta do rejestracji
    public void setPacjent(Pacjent pacjent) {
        this.pacjent = pacjent;
    }

}
