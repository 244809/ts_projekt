package pwr.ib.Servlet;

import pwr.ib.DBUtil.DBUtilNewPatient;
import pwr.ib.Pacjent;
import pwr.ib.Szukanie;
import pwr.ib.Widoki.Terminy;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.util.List;

@WebServlet("/NewPatientServlet")
public class NewPatientServlet extends HttpServlet {

    private DataSource dataSource;
    private DBUtilNewPatient dbUtil;


    public NewPatientServlet() {
        Context initCtx = null;
        try {
            initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");
            dataSource = (DataSource) envCtx.lookup("jdbc/visits_web_app");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws ServletException {
        super.init();
        try {
            dbUtil = new DBUtilNewPatient(dataSource);
            //po inicjacji pacjenta następuje zaktualizowanie bazy danych
            dbUtil.update();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        //rejestracja nowego pacjenta danym z formularza
        String imie = request.getParameter("imie");
        String nazwisko = request.getParameter("nazwisko");
        String telefon = request.getParameter("telefon");
        String dbTelefon = telefon.substring(0, 3) + " " + telefon.substring(3, 6) + " " + telefon.substring(6, 9);
        String login = request.getParameter("login");
        String haslo = request.getParameter("haslo");

        Pacjent pacjent = new Pacjent(imie, nazwisko, dbTelefon, login, haslo);

        dbUtil.setPacjent(pacjent);
        if (dbUtil.searchPatientID()) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/login.html");
            dispatcher.forward(request, response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/registration.html");
            dispatcher.forward(request, response);
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //widok listy wolnych terminów wyszukanych po otrzymaniu odpowiednich parametrów z formularza
        try {
            String miasto = request.getParameter("miasto");
            String lekarz = request.getParameter("specjalizacja");
            String data_od = request.getParameter("data_od");
            String data_do = request.getParameter("data_do");
            Szukanie dane = new Szukanie(miasto, lekarz, data_od, data_do);
            List<Terminy> terminy = dbUtil.getTermins(dane.query());
            request.setAttribute("TERMINS_LIST", terminy);
            request.setAttribute("miasto", miasto);
            request.setAttribute("specjalizacja", lekarz);
            request.setAttribute("data_od", data_od);
            request.setAttribute("data_do", data_do);
            RequestDispatcher dispatcher = request.getRequestDispatcher("/noLoggedPatientsTermin.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }


}
