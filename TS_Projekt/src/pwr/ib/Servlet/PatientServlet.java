package pwr.ib.Servlet;

import pwr.ib.DBUtil.DBUtilPatient;
import pwr.ib.Szukanie;
import pwr.ib.Widoki.Terminy;
import pwr.ib.Widoki.Wizyty;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/PatientServlet")
public class PatientServlet extends HttpServlet {


    private DBUtilPatient dbUtil;
    private final String URL = "jdbc:mysql://localhost:3306/nfz?useSSL=false&allowPublicKeyRetrieval=true&serverTimezone=CET";

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        try {
            dbUtil = new DBUtilPatient(URL);
            //po inicjacji pacjenta następuje zaktualizowanie bazy danych
            dbUtil.update();
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        //logowanie się pacjenta danymi z formularza
        String name = request.getParameter("login");
        String password = request.getParameter("password");
        dbUtil.setRoot("root");
        dbUtil.setRootPassword("Student244809");
        dbUtil.setLogin(name);
        dbUtil.setPassword(password);
        driver();
        if (dbUtil.searchPatientID()) {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/loggedPatientsVisit.jsp");
            List<Wizyty> wizyty = null;
            try {
                wizyty = dbUtil.getVisit(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            request.setAttribute("VISITS_LIST", wizyty);
            dispatcher.forward(request, response);
        } else {
            RequestDispatcher dispatcher = request.getRequestDispatcher("/login.html");
            dispatcher.forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //wszystkie opcje dostępne dla pacjenta
        try {
            String command = request.getParameter("command");
            if (command == null)
                command = "TERMINS";
            switch (command) {
                case "VISITS":
                    listVisit(request, response);
                    break;
                case "TERMINS":
                    listTermin(request, response);
                    break;
                case "ADD":
                    addVisit(request, response);
                    break;
                case "DELETE_VIEW":
                    deleteListVisit(request, response);
                    break;
                case "DELETE":
                    deleteVisit(request, response);
                    break;
                default:
                    listTermin(request, response);
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }

    //usuwanie wybranej wizyty
    private void deleteVisit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id = request.getParameter("visitID");
        dbUtil.deleteVisit(id);
        listVisit(request, response);
    }

    //dodawanie wybranej wizyty
    private void addVisit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String id = request.getParameter("terminID");
        dbUtil.addVisit(id);
        listVisit(request, response);

    }

    //widok z wizytami pacjenta - nadchodzącymi (możliwymi do odwołania)
    private void deleteListVisit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<Wizyty> wizyty = dbUtil.getVisit(false);
        request.setAttribute("VISITS_LIST", wizyty);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/delete_visit.jsp");
        dispatcher.forward(request, response);
    }

    //widok z wizytami pacjenta - wszystkimi
    private void listVisit(HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<Wizyty> wizyty = dbUtil.getVisit(true);
        request.setAttribute("VISITS_LIST", wizyty);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/loggedPatientsVisit.jsp");
        dispatcher.forward(request, response);
    }

    //widok listy wolnych terminów wyszukanych po otrzymaniu odpowiednich parametrów z formularza
    private void listTermin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String miasto = request.getParameter("miasto");
        String lekarz = request.getParameter("specjalizacja");
        String data_od = request.getParameter("data_od");
        String data_do = request.getParameter("data_do");
        Szukanie dane = new Szukanie(miasto, lekarz, data_od, data_do);
        List<Terminy> terminy = dbUtil.getTermins(dane.query());
        request.setAttribute("TERMINS_LIST", terminy);
        request.setAttribute("miasto", miasto);
        request.setAttribute("specjalizacja", lekarz);
        request.setAttribute("data_od", data_od);
        request.setAttribute("data_do", data_do);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/add_visit.jsp");
        dispatcher.forward(request, response);
    }

    private void driver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


}
